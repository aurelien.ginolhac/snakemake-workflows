FROM rocker/r-ubuntu:20.04

MAINTAINER Aurelien Ginolhac <aurelien.ginolhac@uni.lu>

# build with docker build -t ginolhac/snake-rna-seq:0.4 - < Dockerfile
# docker push ginolhac/snake-rna-seq:0.4

LABEL version="0.4"
LABEL description="Docker image to build the snakemake rna-seq singularity"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository -y ppa:openjdk-r/ppa && \
    apt-get update && apt-get install -y \
        build-essential \
        cmake \
        curl \
        libboost-all-dev \
        libbz2-dev \
        libcurl4-openssl-dev \
        liblzma-dev \
        libncurses5-dev \
        libssl-dev \
        libxml2-dev \
        libgd-perl \
        openjdk-8-jdk \
        python3 \
        python3-pip \
        unzip \
        vim-common \
        wget \
        zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

# default to python3 
# better to use https://stackoverflow.com/a/50331137/1395352
RUN ln -sf /usr/bin/python-config /usr/bin/python3.6-config && ln -sf /usr/bin/python3 /usr/bin/python

# htslib
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/htslib/releases/download/1.10.2/htslib-1.10.2.tar.bz2 && \
    tar -xf htslib-1.10.2.tar.bz2 && rm htslib-1.10.2.tar.bz2 && cd htslib-1.10.2 && \
    ./configure --enable-libcurl --enable-s3 --enable-plugins --enable-gcs && \
    make && make install && make clean

# samtools
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/samtools/releases/download/1.10/samtools-1.10.tar.bz2 && \
    tar -xf samtools-1.10.tar.bz2 && rm samtools-1.10.tar.bz2 && cd samtools-1.10 && \
    ./configure --with-htslib=/opt/htslib-1.10.2 && make && make install && make clean
    
# Picard tools
RUN mkdir /opt/picard-tools && \
    wget --no-check-certificate -P /opt/picard-tools/ https://github.com/broadinstitute/picard/releases/download/2.23.1/picard.jar

ARG STAR_VERSION="2.7.4a"   
RUN cd /opt && \
    wget --no-check-certificate https://github.com/alexdobin/STAR/archive/${STAR_VERSION}.tar.gz && \
    tar -xf ${STAR_VERSION}.tar.gz && rm ${STAR_VERSION}.tar.gz && \
    make STAR -C STAR-${STAR_VERSION}/source && make STARlong -C STAR-${STAR_VERSION}/source && \
    mv STAR-${STAR_VERSION}/source/STAR* STAR-${STAR_VERSION}/bin/Linux_x86_64/
ENV PATH /opt/STAR-${STAR_VERSION}/bin/Linux_x86_64:$PATH
# ln -s /opt/STAR-${STAR_VERSION}/bin/Linux_x86_64_static/STAR* /usr/local/bin/

# bedtools
ARG BT_VERSION="2.29.2"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/arq5x/bedtools2/releases/download/v${BT_VERSION}/bedtools-${BT_VERSION}.tar.gz && \
    tar -xf bedtools-${BT_VERSION}.tar.gz && rm bedtools-${BT_VERSION}.tar.gz && \
    cd bedtools2 && make && make install && make clean

# AdapterRemoval
ARG AR_VERSION="2.3.1"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/MikkelSchubert/adapterremoval/archive/v${AR_VERSION}.tar.gz && \
    tar -xf v${AR_VERSION}.tar.gz && rm v${AR_VERSION}.tar.gz && \
    cd adapterremoval-${AR_VERSION} && make && make install && make clean

RUN mkdir /opt/ucsc && \
    wget --no-check-certificate -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bigWigToBedGraph && \
    wget --no-check-certificate -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bedGraphToBigWig && \
    chmod 755 /opt/ucsc/*
ENV PATH /opt/ucsc:$PATH

# BWA
ARG BWA_VERSION="0.7.17"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/lh3/bwa/releases/download/v${BWA_VERSION}/bwa-${BWA_VERSION}.tar.bz2 && \
    tar -xf bwa-${BWA_VERSION}.tar.bz2 && rm bwa-${BWA_VERSION}.tar.bz2 && \
    cd bwa-${BWA_VERSION} && make && mv bwa /usr/local/bin/ && make clean
    
# Bowtie2
ARG BT2_VERSION="2.4.1"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/BenLangmead/bowtie2/releases/download/v${BT2_VERSION}/bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    unzip bowtie2-${BT2_VERSION}-linux-x86_64.zip && rm bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    mv bowtie2-${BT2_VERSION}-linux-x86_64/bowtie2* /usr/local/bin/ && rm -rf bowtie2-${BT2_VERSION}-linux-x86_64

# FastScreen
ENV PERL_MM_USE_DEFAULT=1
RUN cd /opt && \
    wget --no-check-certificate https://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/fastq_screen_v0.14.0.tar.gz && \
    tar -xf fastq_screen_v0.14.0.tar.gz && rm fastq_screen_v0.14.0.tar.gz && perl -MCPAN -e 'install GD::Graph'
ENV PATH /opt/fastq_screen_v0.14.0:$PATH


RUN cd /opt && \
    wget --no-check-certificate https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip && \
    unzip fastqc_v0.11.9.zip && rm fastqc_v0.11.9.zip && chmod +x FastQC/fastqc
ENV PATH /opt/FastQC:$PATH

# python modules
RUN python3 -m pip install snakemake multiqc pandas pyBigWig


# R is 4.0.0
# BiocManager 1.30.10, for BioC 3.11
RUN install2.r --error \
    BiocManager hexbin ggrepel cowplot
# DESeq2 is 1.28.1
# Rsubread is 2.2.2
# apelglm 1.10.0
RUN Rscript -e 'BiocManager::install(c("DESeq2", "Rsubread", "apeglm"), update = FALSE, ask = FALSE)'
    
    # clean up
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    apt-get autoclean && \
    apt-get autoremove -y && rm -rf /var/lib/{dpkg,cache,log}/ # keep /var/lib/apt
   

