

rule star_index:
    input:
        # config["ref"]["species"]
        fasta = "refs/GRCh38.fasta",
        gtf = "refs/GRCh38.gtf"
    output:
        directory("refs/GRCh38")
    message:
        "STAR indexing GRCh38"
    threads:
        12
    params:
        build = config["ref"]["build"],
        extra = ""
    log:
        "logs/star_index_GRCh38.log"
    wrapper:
        "0.47.0/bio/star/index"

rule align:
    input:
        # this function is called as many samples are provided in output
        # like a promise in R
        fq1 = get_fq,
        index="refs/GRCh38"
    output:
        # see STAR manual for additional output files
        "star/{sample}-{unit}/Aligned.sortedByCoord.out.bam",
        "star/{sample}-{unit}/ReadsPerGene.out.tab"
    log:
        "logs/star/{sample}-{unit}.log"
    params:
        # path to STAR reference build index
        build = config["ref"]["build"],
        index = "refs/GRCh38",
        # optional parameters
        extra = "--quantMode GeneCounts {}".format(config["params"]["star"])
    threads: 16
    wrapper:
        "0.47.0/bio/star/align"
