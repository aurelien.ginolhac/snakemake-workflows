def is_single_end(sample, unit):
    return pd.isnull(units.loc[(sample, unit), "fq2"])
    
def get_fq(wildcards):
    if config["trimming"]["skip"]:
        # no trimming, use raw reads
        return units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()
    else:
        # yes trimming, use trimmed data
        if not is_single_end(**wildcards):
            # paired-end sample, return 2 files
            return expand("trimmed_pe/{sample}-{unit}.{group}.fastq.gz",
                          group=[1, 2], **wildcards)
        # single end sample, return only 1 file using pure python format
        return "trimmed_se/{sample}-{unit}.fastq.gz".format(**wildcards)
    
