def get_seq(sample, unit):
    if config["trimming"]["skip"]:
        # no trimming, don't look for setting files
        # return an empty list
        return []
    else:
        if not is_single_end(sample=sample, unit=unit):
            # paired-end sample
            return expand("trimmed_pe/{sample}-{unit}.settings", sample=sample, unit=unit)
        # single end sample
        return "trimmed_se/{sample}-{unit}.settings".format(sample=sample, unit=unit)

def get_raw_fq(wildcards):
    # no trimming, use raw reads
    return units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()

rule multiqc:
    input:
        [get_seq(unit.sample, unit.unit) for unit in units.itertuples()],
        expand("qc/fastqc/{unit.sample}-{unit.unit}_fastqc.zip", unit=units.itertuples()),
        expand("qc/fastq_screen/{unit.sample}-{unit.unit}.fastq_screen.txt", unit=units.itertuples()),
        expand("star/{unit.sample}-{unit.unit}/Aligned.sortedByCoord.out.bam", unit=units.itertuples()),
        "counts/fc_stats.summary"
    output:
        "qc/multiqc_report.html"
    log:
        "logs/multiqc.log"
    wrapper:
        "0.45.1/bio/multiqc"

rule fastqc:
    input:
        sample=get_raw_fq #[get_raw_fq, get_fq] # add trimming skip = True
    output:
        html="qc/fastqc/{sample}-{unit}.html",
        zip="qc/fastqc/{sample}-{unit}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
    group: "raw_qc"
    params: ""
    log:
        "logs/fastqc/{sample}-{unit}.log"
    wrapper:
        "0.45.1/bio/fastqc"

rule fastq_screen:
    input:
        sample=get_raw_fq
    output:
        txt="qc/fastq_screen/{sample}-{unit}.fastq_screen.txt",
        png="qc/fastq_screen/{sample}-{unit}.fastq_screen.png"
    group: "raw_qc"
    #envmodules:
    #    "bio/Bowtie2/2.3.5.1-intel-2019a"
    params:
        fastq_screen_config = {
            'database': {
                'human': {
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Human/Homo_sapiens.GRCh38'},
                'mouse': {
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Mouse/Mus_musculus.GRCm38'},
                'rat':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Rat/Rnor_6.0'},
                'drosophila':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Drosophila/BDGP6'},
                'worm':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Worm/Caenorhabditis_elegans.WBcel235'},
                'yeast':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Yeast/Saccharomyces_cerevisiae.R64-1-1'},
                'arabidopsis':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Arabidopsis/Arabidopsis_thaliana.TAIR10'},
                'ecoli':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/E_coli/Ecoli'},
                'rRNA':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/rRNA/GRCm38_rRNA'},
                'MT':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Mitochondria/mitochondria'},
                'PhiX':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/PhiX/phi_plus_SNPs'},
                'Lambda':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Lambda/Lambda'},
                'vectors':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Vectors/Vectors'},
                'adapters':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Adapters/Contaminants'},
                'mycoplasma':{
                  'bowtie2': '/scratch/users/aginolhac/FastQ_Screen_Genomes/Mycoplasma/mycoplasma'}
                 },
                 'aligner_paths': {'bowtie2': '/usr/local/bin/bowtie2'}
                },
        subset=100000,
        aligner='bowtie2'
    threads: 8
    wrapper:
        "0.45.1/bio/fastq_screen"

rule session:
    output:
        report("results/session.txt", category = "Versions", caption = "../report/session.rst")
    log:
        "logs/session.log"
    script:
        "../scripts/session.R"

