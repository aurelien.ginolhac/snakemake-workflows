
def get_fastq(wildcards):
    return units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()


# adapted from https://github.com/UofABioinformaticsHub/RNAseq_snakemake/blob/master/snakefile.py
rule adapter_removal_pe:
    input:
      get_fastq
    output:
        fastq1="trimmed_pe/{sample}-{unit}.1.fastq.gz",
        fastq2="trimmed_pe/{sample}-{unit}.2.fastq.gz",
        settings="trimmed_pe/{sample}-{unit}.settings",
        discarded="trimmed_pe/{sample}-{unit}.discarded.gz"
    params:
        min_len = config["trimming"]["min_length"],
        adapter1 = config["trimming"]["adapter1"],
        adapter2 = config["trimming"]["adapter2"]
    threads: config["trimming"]["threads"]
    log:
        "logs/adapterremoval/{sample}-{unit}.log"
    shell:
        "AdapterRemoval --file1 {input.fq1} --file2 {input.fq2} "
        "--output1 {output.fastq1} --output2 {output.fastq2} "
        "--settings {output.settings} --discarded {output.discarded} "
        "--adapter1 {params.adapter1} --adapter2 {params.adapter2} "
        "--threads {threads} --gzip --trimqualities --trimns "
        "--minlength {params.min_len}"

# adapted from https://github.com/UofABioinformaticsHub/RNAseq_snakemake/blob/master/snakefile.py
rule adapter_removal_se:
    input:
      get_fastq
    output:
        fastq1="trimmed_se/{sample}-{unit}.fastq.gz",
        settings="trimmed_se/{sample}-{unit}.settings",
        discarded="trimmed_se/{sample}-{unit}.discarded.gz"
    params:
        min_len = config["trimming"]["min_length"],
        adapter1 = config["trimming"]["adapter1"],
    threads: config["trimming"]["threads"]
    log:
        "logs/adapterremoval/{sample}-{unit}.log"
    shell:
        "AdapterRemoval --file1 {input} "
        "--output1 {output.fastq1} "
        "--settings {output.settings} "
        "--discarded {output.discarded} "
        "--adapter1 {params.adapter1} "
        "--threads {threads} "
        "--gzip --trimqualities --trimns "
        "--minlength {params.min_len}"

#rule cutadapt_pe:
#    input:
#        get_fastq
#    output:
#        fastq1="trimmed/{sample}-{unit}.1.fastq.gz",
#        fastq2="trimmed/{sample}-{unit}.2.fastq.gz",
#        qc="trimmed/{sample}-{unit}.qc.txt"
#    params:
#        "-a {} {}".format(config["trimming"]["adapter"], config["params"]["cutadapt-pe"])
#    log:
#        "logs/cutadapt/{sample}-{unit}.log"
#    wrapper:
#        "0.17.4/bio/cutadapt/pe"
#
#
#rule cutadapt:
#    input:
#        get_fastq
#    output:
#        fastq="trimmed/{sample}-{unit}.fastq.gz",
#        qc="trimmed/{sample}-{unit}.qc.txt"
#    params:
#        "-a {} {}".format(config["trimming"]["adapter"], config["params"]["cutadapt-se"])
#    log:
#        "logs/cutadapt/{sample}-{unit}.log"
#    wrapper:
#        "0.17.4/bio/cutadapt/se"
