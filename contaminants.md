
# fastqScreen

## default databases

```
DATABASE        Human   /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Human/Homo_sapiens.GRCh38
DATABASE        Mouse   /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Mouse/Mus_musculus.GRCm38
DATABASE        Rat     /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Rat/Rnor_6.0 
DATABASE        Drosophila      /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Drosophila/BDGP6
DATABASE        Worm    /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Worm/Caenorhabditis_elegans.WBcel235
DATABASE        Yeast   /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Yeast/Saccharomyces_cerevisiae.R64-1-1
DATABASE        Arabidopsis          /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Arabidopsis/Arabidopsis_thaliana.TAIR10
DATABASE        Ecoli   /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/E_coli/Ecoli
DATABASE rRNA /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/rRNA/GRCm38_rRNA 
DATABASE        MT      /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Mitochondria/mitochondria
DATABASE        PhiX         /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/PhiX/phi_plus_SNPs
DATABASE Lambda /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Lambda/Lambda
DATABASE        Vectors /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Vectors/Vectors
DATABASE        Adapters        /home/users/aginolhac/install/fastq_screen_v0.14.0/FastQ_Screen_Genomes/Adapters/Contaminants
```


## mycoplasma

from this [article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4207559/)

95% of cases are caused by the following 6 mycoplasmas: _Mycoplasma arginini, M. fermentans, M. hominis, M. hyorhinis, M. orale and Acholeplasma laidlawii_
