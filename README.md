
Looking for a workflow that manage

- `slurm`
- `singularity`

seems like `snakemake` or `nextflow`

some links:

https://www.biostars.org/p/258436/
https://www.reddit.com/r/bioinformatics/comments/73am0k/ncbi_hackathons_discussions_on_bioinformatics/

excellent exercices: https://github.com/NIH-HPC/snakemake-class
nice template for common rules https://github.com/snakemake-workflows/dna-seq-gatk-variant-calling


## workshop Cambridge

https://slides.com/johanneskoester/snakemake-tutorial#/
https://slides.com/johanneskoester/snakemake-design-patterns#/


- only timestamps are checked, no checksums
- if scripts changed, no automatic re-execution, but list of impacted files availble for re-running
- `snakemake.io.expand()` in python console for debugging
- `--resources sra=3` with `resources sra=1` allows to limit 3 instances of this rule for example
- `snakemake --list-code-changes` to see if any files has changed
- `snakemake --summary` provides a nice tabular table
- function `get_fq` is called as many samples are provided in output, like a promise in R
- more importantly, `get_fq()` execute the call, while `get_fq` just create a pointer
- `pipe()` jobs to avoid writing on the disk
- handling reference, to combine with with the `--cache get-genome bwa_index` with `export SNAKEMAKE_OUTPUT_CACHE=/mnt/snakemake-cache/`
- Split-Apply-Combine
- replace `rule` by `checkpoint` to force re-evaluation of the DAG, like dynamic branching
- put all rules / Snakefile in a folder `workflow` and *.tsv, config.yaml in a `config` folder

add a validate_fq step and and a shell directive to exit 1 with an error message

### references

```{bash}
export SNAKEMAKE_OUTPUT_CACHE=/scratch/users/aginolhac/smk_refs/
snakemake --cache get_genome get_annotation star_index
```

### expand and pandas

- all combinations:

```{python}
import snakemake
import pandas as pd
units = pd.read_table("units.tsv", dtype=str).set_index(["sample", "unit"], drop=False)
snakemake.io.expand('{sample}-{unit}', sample=units['sample'], unit=units['unit']) 
```

expand("qc/{sample}-{unit}.fastq_screen.txt", sample=units["sample"], unit=units["unit"])

returns:
```
['C1-rep1', 'C1-rep2', 'C1-rep3', 'C1-rep1', 'C1-rep2', 'C1-rep3', 'C2-rep1', 'C2-rep2', 'C2-rep3', 'C2-rep1', 'C2-rep2', 'C2-rep3', 'C3-rep1', 'C3-rep2', 'C3-rep3', 'C3-rep1', 'C3-rep2', 'C3-rep3', 'G1-rep1', 'G1-rep2', 'G1-rep3', 'G1-rep1', 'G1-rep2', 'G1-rep3', 'G2-rep1', 'G2-rep2', 'G2-rep3', 'G2-rep1', 'G2-rep2', 'G2-rep3', 'G3-rep1', 'G3-rep2', 'G3-rep3', 'G3-rep1', 'G3-rep2', 'G3-rep3']
```


or just the existing pairs

```{python}
import snakemake
import pandas as pd
units = pd.read_table("units.tsv", dtype=str).set_index(["sample", "unit"], drop=False)
snakemake.io.expand("qc/{unit.sample}-{unit.unit}.fastq_screen.txt", unit=units.itertuples())
```

returns:
```
['qc/C1-rep1.fastq_screen.txt', 'qc/C2-rep2.fastq_screen.txt', 'qc/C3-rep3.fastq_screen.txt',
'qc/G1-rep1.fastq_screen.txt', 'qc/G2-rep2.fastq_screen.txt', 'qc/G3-rep3.fastq_screen.txt']
```

### questions

- does snakemake check the presence and versions of softwares if no envs are available?
- is iris cluster  `--drmaa` compatible?

## RNA-seq

https://github.com/snakemake-workflows/rna-seq-star-deseq2

For RNA-seq, look into this tutorial https://github.com/griffithlab/rnaseq_tutorial



#### install

updating the `conda` from Sarah Peter's tutorial
and installing `snakemake` as in the [RFTM](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)

```
module load tools/Singularity
module load swenv/default-env/devel && module load lang/Python/3.7.2-GCCcore-8.2.0 
conda update conda
conda install -c bioconda -c conda-forge snakemake
```

for testing, fetch chr21 and fastq in `https://github.com/snakemake-workflows/ngs-test-data/tree/master/ref`

and generate the STAR index (using STAR_2.6.1d)

```
seqtk subseq GRCm38.p4.genome.fa <(echo "chr17") | seqtk seq -l 80 > ~/install/rna-seq-star-deseq2/ref/mouse.chr17.fa
grep -e '^#' -e '^chr17' gencode.vM9.annotation.gtf > ~/install/rna-seq-star-deseq2/ref/annotation.chr17.gtf
mkdir ref/chr17Index
STAR --runThreadN 8 --runMode genomeGenerate \
  --genomeDir ref/chr17Index \
  --genomeSAindexNbases 12 \ # 14 too high for fake one chr
  --genomeFastaFiles ref/mouse.chr17.fa \
  --sjdbGTFfile ref/annotation.chr17.gtf \
  --sjdbOverhang 74
```

test with 
```
srun --cpu-bind=none -p interactive -t 0-1:00:0 -c 8 --pty bash -i
# using conda
snakemake --use-conda --cores 8
# or using singularity
module load tools/Singularity
conda activate py3.7
module load tools/Singularity
snakemake --cores 12 --use-envmodules --use-singularity --singularity-args "--bind /scratch/users/aginolhac:/scratch/users/aginolhac"
```


Singularity bind automatically `/home/$USER`, `/tmp`, and `$PWD` into your container at runtime. Relative paths are fine as long as they keep `$PWD` as top level.

### dependencies

```
snakemake --dag | dot -Tpdf > dag.pdf
```

![](dag.png)


### collections of wrappers 

for bioinfo tools are in https://snakemake-wrappers.readthedocs.io/en/stable/wrappers/


### adding new input files


>Snakemake does not automatically rerun jobs when new input files are added

from https://snakemake.readthedocs.io/en/stable/tutorial/advanced.html#step-3-input-functions

display steps to be redone:

```
snakemake --list-input-changes
```

```
snakemake --use-singularity --cores 8 --forcerun $(snakemake --list-input-changes)
```


### use `slurm` profile

`/home/users/aginolhac/.cookiecutters/slurm/tests/cluster-config.yaml`

config files are in `~/.config/snakemake/slurm/`

```
snakemake --use-singularity --profile slurm -j 8 --cluster "sbatch p batch -n 1 -c 8 --time=00:10:00"
```

### simpler attempt to profile

using this [exercise05](https://github.com/NIH-HPC/snakemake-class/tree/master/exercise05)

```
snakemake --profile ./config/slurm_profile --jobs 8 --cluster-config=./config/slurm_profile/iris.yaml --local-cores 2
```

works by submitting jobs itself.

Important: `localrules` are NOT submitted like `clean`, `all`.
And they use `--local-cores` threads. Since we are using singularity, and it is not available 
on the access node, we should have `snakemake` running on a 2 cores `long` job that will submit the inner jobs on `batch`

with references caching

```
srun -p long -t 0-4:00:0 -c 2 --pty bash -i
module load tools/Singularity
conda activate py3.7
export SNAKEMAKE_OUTPUT_CACHE=/scratch/users/aginolhac/smk_refs/
snakemake --cores 12 --use-singularity --singularity-args "--bind /scratch/users/aginolhac:/scratch/users/aginolhac"  --cache get_genome get_annotation star_index
```

## new dataset using Schuy data, C3 knock-down

```
cd ~/lsru/rna-seq/Bonn-data
for f in C1 C2 C3 G1 G2 G3
  do samtools view -b ${f}_sequence_Adap_q30_Trim_clean*Aligned.sortedByCoord.out.bam chr17 | samtools fastq - | gzip > ${f}_chr17.fastq.gz
done
```


## using input files from scratch

singularity only mount `home` by default

```
snakemake --cores 2 --use-singularity --singularity-args "--bind /scratch/users/aginolhac:/scratch/users/aginolhac"
```

## create test Yeast data 

## fetching data

contrast of interest: BY_Gis1_vs_BY_WT in rna-seq/180603/2019-10

chosen chrs: IV and XVI


```{bash}
SAMPLES="
BY_A_Lasse_truncated.worRNA.fastqAligned.sortedByCoord.out_q30_nodup.bam
BY_B_Lasse_truncated.worRNA.fastqAligned.sortedByCoord.out_q30_nodup.bam
BY_Gis1_A_truncated.worRNA.fastqAligned.sortedByCoord.out_q30_nodup.bam
BY_Gis1_C_truncated.worRNA.fastqAligned.sortedByCoord.out_q30_nodup.bam
"

for f in $SAMPLES
  do echo $f
  samtools view $f chrIV | cut -f 1 > ${f/_truncated.worRNA.fastqAligned.sortedByCoord.out_q30_nodup.bam/_IV.txt}
done

for list in *.txt
  do echo $list
  echo ${list/_IV.txt/*_R1_001.fastq.gz}
  seqtk subseq -l 120  ${list/_IV.txt/*_R1_001.fastq.gz} \
    $list | gzip > ${list/.txt/.fastq.gz}
done
```

## create docker image

based on [rocker](https://github.com/rocker-org/rocker)
with elements from the [gtex-pipeline](https://github.com/broadinstitute/gtex-pipeline/blob/master/rnaseq/Dockerfile)


see `Dockerfile`

`docker build -t ginolhac/snake-rna-seq:0.2 .`


modify

```
docker run -ti ginolhac/snake-rna-seq:0.2 /bin/bash
```

get commit hash

```
docker ps
```

then push to docker hub

```
docker commit 8025b8260d9d  ginolhac/snake-rna-seq
docker push ginolhac/snake-rna-seq
```



## oxford nanopore

details in `~/Work/170607/nanopype.md`

promising but see [issue](https://github.com/giesselmann/nanopype/issues/5) with `nanopype`
